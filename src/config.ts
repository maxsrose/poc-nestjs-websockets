import { readFile } from 'node:fs/promises';
import { join } from 'node:path';
import { parse } from 'yaml';
import { RabbitMQExchangeConfig } from '@golevelup/nestjs-rabbitmq/lib/rabbitmq.interfaces';
import { template } from 'lodash';

export const handlerNames = {
  bonusWallet: 'bonusWallet',
} as const;

type Handlers = keyof typeof handlerNames;

type Handler = {
  exchange: string;
  routingKey: string[];
  queue?: string;
  autoDelete: true;
};

export type MessagingConfig = {
  url: string;
  franchises: Array<{ id: number }>;
  exchanges: Array<RabbitMQExchangeConfig>;
  handlers: Record<Handlers, Handler>;
};

export type AppConfig = {
  messaging: MessagingConfig;
};

export default async (): Promise<AppConfig> => {
  const settingsFile = await readFile(
    join(__dirname, 'settings.yaml'),
    'utf-8',
  );

  const config = parse(settingsFile) as AppConfig;

  config.messaging.handlers = Object.fromEntries(
    Object.entries(config.messaging.handlers).map(([k, v]) => {
      if (v.queue) v.queue = `${v.queue}-${new Date()}`;
      v.autoDelete = true;

      v.routingKey = v.routingKey.flatMap((rk) =>
        config.messaging.franchises.map((f) => template(rk)(f)),
      );

      return [k, v];
    }),
  ) as Record<Handlers, Handler>;

  return config;
};
