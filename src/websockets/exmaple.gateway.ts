import {
  ConnectedSocket,
  MessageBody,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Server, Socket } from 'socket.io';
import { MessagePublisherService } from '../global/message-publisher.service';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class ExmapleGateway implements OnGatewayInit, OnGatewayDisconnect {
  #logger = new Logger('ExmapleGateway');

  constructor(private readonly publisher: MessagePublisherService) {}

  @SubscribeMessage('join')
  async join(
    @MessageBody() data: { playerId: string }, //: Observable<WsResponse<{ playerId: number; data: unknown }>> {
    @ConnectedSocket() socket: Socket,
  ) {
    this.#logger.log(`Client ${data.playerId} ${JSON.stringify(data)} joined!`);
    await socket.join(`${data.playerId}`);
  }

  @SubscribeMessage('get-ip')
  async getIp(@ConnectedSocket() socket: Socket) {
    socket.emit(
      'user-ip',
      socket.handshake.headers['x-real-ip'] ||
        socket.request.socket.remoteAddress,
    );
  }

  async handleDisconnect(socket: Socket) {
    for (const room of new Set(socket.rooms)) {
      await socket.leave(room);
    }

    this.#logger.log('Disconnected');
  }

  afterInit(server: Server) {
    this.publisher.eventListener$.subscribe((message) => {
      server.in(message.room).emit(message.event, message.message);
    });
  }
}
