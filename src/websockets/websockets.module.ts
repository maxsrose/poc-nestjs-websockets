import { Module } from '@nestjs/common';
import { ExmapleGateway } from './exmaple.gateway';

@Module({
  providers: [ExmapleGateway],
})
export class WebsocketsModule {}
