import {
  HealthCheckError,
  HealthIndicator,
  HealthIndicatorResult,
} from '@nestjs/terminus';
import { Injectable } from '@nestjs/common';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

@Injectable()
export class RabbitHealth extends HealthIndicator {
  constructor(private readonly amqpConnection: AmqpConnection) {
    super();
  }

  async isHealthy(key: string): Promise<HealthIndicatorResult> {
    const result = this.getStatus(key, this.amqpConnection.connected);

    if (this.amqpConnection.connected) return result;

    throw new HealthCheckError(
      `${key} Failed`,
      'Could not connect to RabbitMQ',
    );
  }
}
