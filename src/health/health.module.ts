import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health.controller';
import { RabbitHealth } from './checks/rabbit.health';
import { MessagingModule } from '../messaging/messaging.module';

@Module({
  imports: [TerminusModule, MessagingModule],
  controllers: [HealthController],
  providers: [RabbitHealth],
})
export class HealthModule {}
