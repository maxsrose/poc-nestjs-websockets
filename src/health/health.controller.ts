import { Controller, Get } from '@nestjs/common';
import { HealthCheck, HealthCheckService } from '@nestjs/terminus';
import { RabbitHealth } from './checks/rabbit.health';

@Controller('health')
export class HealthController {
  constructor(
    private readonly health: HealthCheckService,
    private readonly rabbitHealth: RabbitHealth,
  ) {}

  @Get()
  @HealthCheck()
  check() {
    return this.health.check([() => this.rabbitHealth.isHealthy('rabbit')]);
  }
}
