import { Global, Module } from '@nestjs/common';
import { MessagePublisherService } from './message-publisher.service';

@Global()
@Module({
  exports: [MessagePublisherService],
  providers: [MessagePublisherService],
})
export class SharedModule {}
