import { Injectable } from '@nestjs/common';
import { Subject } from 'rxjs';

@Injectable()
export class MessagePublisherService {
  #events$ = new Subject<{
    room: string;
    event: string;
    message?: Record<string, unknown>;
  }>();

  get eventListener$() {
    return this.#events$.asObservable();
  }

  public publishMessage(room: string, message?: Record<string, unknown>) {
    this.#events$.next({ room, event: 'message', message });
  }
}
