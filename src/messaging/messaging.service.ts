import { Injectable, Logger } from '@nestjs/common';
import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { MessagePublisherService } from '../global/message-publisher.service';
import { handlerNames } from '../config';

@Injectable()
export class MessagingService {
  #logger = new Logger('MessagingService');

  constructor(private messaging: MessagePublisherService) {}

  @RabbitSubscribe({
    name: handlerNames.bonusWallet,
  })
  public async handleBonusWalletChange(message: {
    PlayerID: string;
    FranchiseId: number;
    BalanceBefore: number;
    BalanceAfter: number;
  }) {
    this.#logger.log(
      `Received player ${message.PlayerID} bonus balance update`,
    );

    const data = {
      balance_before: message.BalanceBefore,
      balance_after: message.BalanceAfter,
    };

    this.messaging.publishMessage(
      `${message.FranchiseId}-${message.PlayerID}`,
      data,
    );
  }
}
