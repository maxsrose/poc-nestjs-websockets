import { Module } from '@nestjs/common';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';
import { MessagingService } from './messaging.service';
import { ConfigService } from '@nestjs/config';
import { MessagingConfig } from '../config';

@Module({
  imports: [
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      inject: [ConfigService<{ messaging: MessagingConfig }, true>],
      useFactory: (
        configService: ConfigService<{ messaging: MessagingConfig }, true>,
      ) => {
        const config = configService.get('messaging', { infer: true });

        return {
          uri: config.url,
          exchanges: config.exchanges,
          handlers: config.handlers,
        };
      },
    }),
  ],
  providers: [MessagingService],
  exports: [RabbitMQModule],
})
export class MessagingModule {}
