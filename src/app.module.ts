import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagingModule } from './messaging/messaging.module';
import { WebsocketsModule } from './websockets/websockets.module';
import { SharedModule } from './global/shared.module';
import { HealthModule } from './health/health.module';
import { ConfigModule } from '@nestjs/config';
import Config from './config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [Config],
    }),
    MessagingModule,
    WebsocketsModule,
    SharedModule,
    HealthModule,
  ],
  providers: [AppService, SharedModule],
})
export class AppModule {}
